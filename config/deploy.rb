# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, "test_app"
set :repo_url, "https://gitlab.com/joel_Garcia/capistrano-rails-test.git"
set :linked_dirs, fetch(:linked_dirs, []).push('log',
  'tmp/pids', 'tmp/cache', 'tmp/sockets',
  'vendor/bundle', 'public/system', 'public/uploads')
set :rvm_ruby_version, '2.4.1'
set :passenger_restart_with_touch, true
